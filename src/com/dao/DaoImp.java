package com.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import com.dbconfig.DBConnection;
import com.model.Infraction;
import com.model.Permis;

public class DaoImp extends DBConnection implements Dao {

	@Override
	public Conducteur getConducteur(String cni) {

		Conducteur conducteur = null;

		try {
			String query = "SELECT * FROM conducteurs WHERE cin=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, cni);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				conducteur = new Conducteur(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7));
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return conducteur;
	}

	@Override
	public Permis getPermis(Conducteur conducteur) {

		Permis permis = null;

		try {
			String query = "SELECT * FROM permis WHERE cin=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, conducteur.getCni());
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
//				 id_permis 	date 	points 	etat 	cin
				permis = new Permis(resultSet.getInt(1), resultSet.getDate(2), resultSet.getInt(3),
						resultSet.getString(4), resultSet.getString(5));
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return permis;
	}

	@Override
	public boolean insertInfraction(Infraction i) {
		String query = "INSERT INTO `infraction` (`id_infraction`, `date`, `id_type`, `cin`) VALUES (NULL, ?, ?, ?)";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setDate(1, i.getDate());
			preparedStatement.setInt(2, i.getTypeInfraction());
			preparedStatement.setString(3, String.valueOf(i.getCni()));

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			System.err.println(e);
			return true;
		}

		disconnect();
		return true;
	}

	@Override
	public boolean estProbatoire(Permis p) {

		String res = "";
		try {
			String query = "SELECT etat FROM permis WHERE cin=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, p.getCni());
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				res = resultSet.getString("etat");
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return res.equals("probatoire");
	}

	@Override
	public boolean estGrave(int typeInfraction) {
		boolean res = false;
		try {
			String query = "SELECT estGrave FROM typeinfraction WHERE id_type=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setInt(1, typeInfraction);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				res = resultSet.getBoolean("estGrave");
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return res;
	}

	@Override
	public boolean majSoldePoint(Permis p, int typeInfraction) {
		int solde = p.getSolde();
		int penalite = recupererPenalitePoint(typeInfraction);

		int newSolde = solde - penalite;

		String query = "UPDATE permis SET points=? WHERE cin=?";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setInt(1, newSolde);
			preparedStatement.setString(2, p.getCni());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			System.err.println(e);
			return false;
		}

		disconnect();
		return true;
	}

//	private int recupererSolde(Permis p) {
//		int res = 0;
//		try {
//			String query = "SELECT points FROM permis WHERE cin=?";
//			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
//			preparedStatement.setString(1, p.getCni());
//			ResultSet resultSet = preparedStatement.executeQuery();
//			if (resultSet.next()) {
//				res = resultSet.getInt("points");
//			}
//
//			if (preparedStatement != null)
//				preparedStatement.close();
//			if (resultSet != null)
//				resultSet.close();
//
//		} catch (Exception e) {
//			System.out.println("Connect to database: " + e);
//		}
//
//		disconnect();
//		return res;
//	}

	private int recupererPenalitePoint(int typeInfraction) {
		int res = 0;
		try {
			String query = "SELECT nbpoints FROM typeinfraction WHERE id_type=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setInt(1, typeInfraction);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				res = resultSet.getInt("nbpoints");
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();
		return res;
	}

	@Override
	public boolean verifier2AnsSansInfraction(Conducteur conducteur) {

		if (conducteur == null)
			return false;

		final long DEUX_ANS_EN_SECONDE = 63072000;

		Permis permis = getPermis(conducteur);

		int solde = permis.getSolde();

		if (solde == 100)
			return false;

		Infraction dernierInfraction = recupererDernierInfraction(conducteur);

		Date dernierDateInfraction = dernierInfraction.getDate();

		long timeDiffrence = Date.valueOf(LocalDate.now()).getTime() - dernierDateInfraction.getTime();
//		System.out.println((timeDiffrence / 1000l) / (24 * 365 * 3600));
		if (timeDiffrence / 1000l > DEUX_ANS_EN_SECONDE) {
			restorerSolde(permis);
			return true;
		}
		return false;

	}

	private void restorerSolde(Permis permis) {
		String query = "UPDATE permis SET points=100 WHERE cin=?";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, permis.getCni());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			System.err.println(e);
		}

		disconnect();
	}

	private Infraction recupererDernierInfraction(Conducteur conducteur) {
		Infraction infraction = null;
		try {
			String query = "SELECT * FROM infraction WHERE cin=? ORDER BY id_infraction DESC LIMIT 1";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, conducteur.getCni());
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				infraction = new Infraction(resultSet.getInt(1), resultSet.getDate(2), resultSet.getInt(3),
						resultSet.getString(4));
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return infraction;
	}

	@Override
	public void ajouterAvertissement(Conducteur conducteur, int typeInfraction) {

		int penalite = recupererPenalitePoint(typeInfraction);

		String query = "INSERT INTO avertissement (id, contenu, date, id_conducteur) VALUES (NULL, ?, ?, ?)";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, "Votre solde a ete reduit par " + penalite + " points");
			preparedStatement.setDate(2, Date.valueOf(LocalDate.now()));
			preparedStatement.setInt(3, conducteur.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			System.err.println(e);
		}

		disconnect();
	}

	@Override
	public boolean soldeSuffisant(Permis permis) {
		return permis.getSolde() > 20;
	}

	@Override
	public boolean eliminerPermis(Permis permis) {
		// TODO Auto-generated method stub
		String query = "UPDATE permis SET etat='retire', points=0 WHERE id_permis=?";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setInt(1, permis.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			System.err.println(e);
			return false;
		}

		disconnect();
		return true;

	}

	public static void main(String[] args) {
//		System.out.println(new DaoImp().recupererDernierInfraction("pb191967"));
//		System.out.println(new DaoImp().getPermis(new DaoImp().getConducteur("ae2536")));
		DaoImp daoImp = new DaoImp();

//		daoImp.verifier2AnsSansInfraction(daoImp.getConducteur("pb191967"));
//		daoImp.ajouterAvertissement(daoImp.getConducteur("pb191967"), 8);

		daoImp.eliminerPermis(daoImp.getPermis(daoImp.getConducteur("ae2536")));
	}

	@Override
	public boolean envoyerAvisDeRetrait(Conducteur conducteur) {
		if (conducteur != null) {
			if (conducteur.getEmail() != null) {
				sendEmail();
				return true;
			}
		}
		return false;
	}

	private void sendEmail() {
		System.out.println("L'avis a ete envoye");
	}

}
