package com.dao;

import com.model.Infraction;
import com.model.Permis;

public interface Dao {

	public Conducteur getConducteur(String cni);

	public Permis getPermis(Conducteur conducteur);

	public boolean insertInfraction(Infraction i);

	public boolean estProbatoire(Permis p);

	public boolean estGrave(int typeInfraction);

	public boolean majSoldePoint(Permis p, int typeInfraction);

	public boolean verifier2AnsSansInfraction(Conducteur conducteur);
	
	public void ajouterAvertissement(Conducteur conducteur, int typeInfraction);
	
	public boolean soldeSuffisant(Permis permis);
	
	public boolean eliminerPermis(Permis permis);
	
	public boolean envoyerAvisDeRetrait(Conducteur conducteur);

}
