package com.model;

import java.sql.Date;
import java.time.LocalDate;

public class Infraction {
	private int id;
	private Date date;
	private int typeInfraction;
	private String cni;

	// constructor when we read from database
	public Infraction(int id, Date date, int typeInfraction, String cni) {
		this.id = id;
		this.date = date;
		this.typeInfraction = typeInfraction;
		this.cni = cni;
	}

	// constructor when we want to insert to the database
	public Infraction(String cni, int typeInfraction) {
		this.typeInfraction = typeInfraction;
		this.cni = cni;
		date = Date.valueOf(LocalDate.now());
	}

	public String getCni() {
		return cni;
	}

	public void setCni(String cni) {
		this.cni = cni;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTypeInfraction() {
		return typeInfraction;
	}

	public void setTypeInfraction(int typeInfraction) {
		this.typeInfraction = typeInfraction;
	}

	@Override
	public String toString() {
		return "ID: " + id + ", Date: " + date + ", Type: " + typeInfraction + ", CNI: " + cni;
	}
}
