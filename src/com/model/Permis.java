package com.model;

import java.sql.Date;
import java.time.LocalDate;

public class Permis {
	

	private int id;
	private Date date;
	private int solde;
	private String etat;
	private String cni;

//	 id_permis 	date 	points 	etat 	cin
	public Permis(int id, Date date, int solde, String etat, String cin) {

		this.id = id;
		this.date = date;
		this.solde = solde;
		this.etat = etat;
		this.cni = cin;
	}

	public Permis(Date date, int solde, String etat, String cin) {

		this.date = Date.valueOf(LocalDate.now());
		this.solde = solde;
		this.etat = etat;
		this.cni = cin;
	}
	
	
	public Permis(String cni) {
		this.cni = cni;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getSolde() {
		return solde;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getCni() {
		return cni;
	}

	public void setCni(String cni) {
		this.cni = cni;
	}

}