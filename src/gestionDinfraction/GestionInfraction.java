package gestionDinfraction;

import com.dao.DaoImp;
import com.model.Infraction;
import com.model.Permis;

public class GestionInfraction {

	DaoImp daoImp;

	public GestionInfraction() {
		daoImp = new DaoImp();
	}

	public boolean ajoutInfraction(String cni, int typeInfraction) {

		System.out.println("ajoutInfraction ...");

		return daoImp.insertInfraction(new Infraction(cni, typeInfraction));
	}

	public boolean estProbatoire(String cni) {
		boolean res = daoImp.estProbatoire(new Permis(cni));
		System.out.println("est probatoire: " + res);
		return res;
	}

	public boolean estGrave(int typeInfraction) {
		boolean res = daoImp.estGrave(typeInfraction);
		System.out.println("est grave: " + res);
		return res;
	}

	public boolean majSoldePoint(String cni, int typeInfraction) {
		boolean res = daoImp.majSoldePoint(daoImp.getPermis(daoImp.getConducteur(cni)), typeInfraction);
		System.out.println("MAJ solde permis:" + res);
		return res;
	}

	public boolean verifier2AnsSansInfraction(String cni) {
		System.out.println("verifier2AnsSansInfraction: I've been called by bonita -->" + cni);
		return daoImp.verifier2AnsSansInfraction(daoImp.getConducteur(cni));
	}

	public void ajouterAvertissement(String cni, int typeInfraction) {
		daoImp.ajouterAvertissement(daoImp.getConducteur(cni), typeInfraction);
	}

	public boolean soldeSuffisant(String cni) {
		System.out.println("verification du solde...");
		return daoImp.soldeSuffisant(daoImp.getPermis(daoImp.getConducteur(cni)));
	}

	public boolean eliminerPermis(String cni) {
		return daoImp.eliminerPermis(daoImp.getPermis(daoImp.getConducteur(cni)));
	}

	public boolean envoyerAvisDeRetrait(String cni) {
		return daoImp.envoyerAvisDeRetrait(daoImp.getConducteur(cni));
	}

//	public static void main(String[] args) {
////		new GestionInfraction().majSoldePoint("pb191967", 8);
//	}
}
